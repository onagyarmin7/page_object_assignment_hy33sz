﻿using HY33SZ.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HY33SZ.Widgets
{
    //Driver calls only in the widgets
    class SportNewsWidget : WidgetBase
    {
        public SportNewsWidget(IWebDriver webDriver) : base(webDriver) { }

        IWebElement HeaderObject => Driver.FindElement(By.ClassName("rovat-nev"));

        public override string GetHeaderName()
        {
            //Using expicit wait
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            wait.Until(t => HeaderObject.Displayed);

            //Validate the visibility of an item
            if (HeaderObject.Displayed)
                return HeaderObject.Text;
            else
                return String.Empty;
        }
    }
}
