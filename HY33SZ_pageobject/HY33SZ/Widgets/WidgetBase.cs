﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HY33SZ.Widgets
{
    class WidgetBase
    {
        protected IWebDriver Driver;

        public WidgetBase(IWebDriver webDriver)
        {
            Driver = webDriver;
        }

        public virtual string GetHeaderName()
        {
            return String.Empty;
        }

    }
}
