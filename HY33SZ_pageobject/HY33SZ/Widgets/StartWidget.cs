﻿using HY33SZ.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HY33SZ.Widgets
{
    //Driver calls only in the widgets
    class StartWidget : WidgetBase
    {
        public StartWidget(IWebDriver webDriver) : base(webDriver) { }

        //CSS selectors because By.ClassName cannot contain whitespace
        public IWebElement AbroadNewsButton => Driver.FindElement(By.CssSelector("a[class='rovat-link navi_kulfold']"));
        public IWebElement SportNewsButton => Driver.FindElement(By.CssSelector("a[class='rovat-link navi_sport']"));
        public IWebElement EconomyNewsButton => Driver.FindElement(By.CssSelector("a[class='rovat-link navi_gazdasag']"));
        
        public IWebElement CookieQuestionWindow => Driver.FindElement(By.CssSelector("div[class='qc-cmp2-footer qc-cmp2-footer-overlay qc-cmp2-footer-scrolled']"));
        public IWebElement AcceptCookiesButton => Driver.FindElement(By.CssSelector("button[class=' css-k8o10q']"));

        public enum PageName
        {
            Abroad, Sport, Economy
        }

        public WidgetBase GoToPage(PageName newPage)
        {
            switch (newPage)
            {
                case PageName.Abroad: 
                    return ClickOnTheButton(AbroadNewsButton, typeof(AbroadNewsWidget));
                case PageName.Economy:
                    return ClickOnTheButton(EconomyNewsButton, typeof(EconomyNewsWidget));
                case PageName.Sport:
                    return ClickOnTheButton(SportNewsButton, typeof(SportNewsWidget));
                default: return new WidgetBase(Driver);
            }
        }

        private WidgetBase ClickOnTheButton(IWebElement newPageButton, Type pageType)
        {
            if (AcceptCookies())
            {
                //Using expicit wait
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
                wait.Until(t => newPageButton.Displayed);

                newPageButton.Click();
                return Activator.CreateInstance(pageType, Driver) as WidgetBase;
            }

            return new WidgetBase(Driver);        
        }
        
        private bool AcceptCookies()
        {
            Driver.Manage().Window.FullScreen();

            //Using expicit wait
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
            wait.Until(t => CookieQuestionWindow.Displayed);

            //Validate the visibility of an item
            if (AcceptCookiesButton.Displayed)
            {
                AcceptCookiesButton.Click();
                return true;
            }

            return false;
        } 
    }
}
