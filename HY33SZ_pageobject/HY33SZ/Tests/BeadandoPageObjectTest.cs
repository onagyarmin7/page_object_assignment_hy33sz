﻿using HY33SZ.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using static HY33SZ.Widgets.StartWidget;

namespace HY33SZ.Tests
{
    //---------- [ REQUIREMENTS ] ----------------------------------------------
    // REQ 1) Create a BeadandoPageObjectTests.cs file                  -> Done |
    // REQ 2) Inherit BeadandoPageObjectTests.cs file from TestBase     -> Done |
    // REQ 3) Inherit page(s) from BasePage class                       -> Done |
    // REQ 4) Create 4 widget classes for the page                      -> Done |
    // REQ 5) Use comprehensive and communicative names                 -> Done |
    // REQ 6) Use Data Driven approach to run 3 separate tests          -> Done |
    //--------------------------------------------------------------------------

    class BeadandoPageObjectTest : TestBase
    {
        //REQ 6) Use Data Driven approach to run 3 separate tests
        [Test, TestCaseSource("GetSites")]
        public void PageObjectSearch(PageName pName, String expectedHeader)
        {           
            //Using builder design pattern
            var headerResult =
                StartPage.Navigate(Driver)
                .GetStartWidget()
                .GoToPage(pName)
                .GetHeaderName();

            //This for the plus points. If we remove this line all tests will be green.
            if (IsScreenshotNeeded()) Assert.Fail("This is the third test so it needs to fail.");

            Assert.AreEqual(headerResult,expectedHeader);
        }

        static IEnumerable GetSites()
        {
            var doc = XElement.Load(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory) + "\\sites.xml");
            return
                from vars in doc.Descendants("SiteInfo")
                let name = Enum.Parse(typeof(PageName), vars.Attribute("pageName").Value, true)
                let header = vars.Attribute("header").Value
                select new object[] { name, header };
        }

        int testCounter = 0;
        public bool IsScreenshotNeeded() // If it's the third test it takes a screenshot and return with true
        {
            testCounter++;
            if (testCounter == 3)
            {
                testCounter = 0;
                TakeAScreenShot();
                return true;
            }
            return false;            
        }

        private void TakeAScreenShot()
        {
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string screenshotName = baseDirectory + TestContext.CurrentContext.Test.Name.Replace(@"""", String.Empty) + "_" +
                DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + "_error.png";

            Console.WriteLine(screenshotName);
            var screenshot = ((ITakesScreenshot)Driver).GetScreenshot();
            screenshot.SaveAsFile(screenshotName, ScreenshotImageFormat.Png);
        }

    }
}
