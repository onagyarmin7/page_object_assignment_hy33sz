﻿using HY33SZ.Widgets;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HY33SZ.Pages
{
    //REQ 3) Inherit page(s) from BasePage class
    class StartPage : BasePage
    {
        public StartPage(IWebDriver webDriver) : base(webDriver) { }
    
        public static StartPage Navigate(IWebDriver webDriver)
        {
            webDriver.Url = "https://index.hu/";
            return new StartPage(webDriver);
        }

        public StartWidget GetStartWidget()
        {
            return new StartWidget(Driver);
        }  
    }
}
